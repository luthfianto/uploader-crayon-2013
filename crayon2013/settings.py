# Django settings for crayon2013 project.
import os
TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\','/'),)
SITE_ID=1

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING
SECRET_KEY = 'ts-xvno-6w7mu20ie7k(%1v&h&&&c+cl6uv^yp084o&!*7058v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = DEBUG
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'crayon2013.db',
    }
}

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'uploader',
    'south',
    # ,'widget_tweaks',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
     'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'crayon2013.urls'
WSGI_APPLICATION = 'crayon2013.wsgi.application'

# Internationalization
TIME_ZONE = 'Asia/Jakarta'
LANGUAGE_CODE = 'id'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files
MEDIA_ROOT = 'media/'
MEDIA_URL = '/media/'
STATIC_ROOT = 'static/'
STATIC_URL = '/static/'

CONN_MAX_AGE = None

LOGIN_REDIRECT_URL='/uploader'
LOGOUT_REDIRECT_URL='/uploader'