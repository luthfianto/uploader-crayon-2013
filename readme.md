### Preparasi ###

- Install [Python 3](http://www.python.org/getit/)
- Install pip
- pip install django
- pip install Pillow (dibutuhkan ImageField)
- pip install South (untuk migrasi. tak dibutuhkan di django 1.7, comment saja di settings.py)

### Kalau ingin databasenya mulai dari awal ###

- Hapus file .db
- Masuk ke folder crayon2013 hingga terlihat manage.py
- Tahan Shift dan klik kanan, lalu open command prompt
- python manage.py syncdb

### Eksekusi ###

- Di folder crayon2013, python manage.py runserver ipaddressdijaringan:80