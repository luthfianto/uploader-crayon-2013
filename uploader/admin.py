from django.contrib import admin
from uploader.models import *

class PesertaAdmin(admin.ModelAdmin):
    list_display = ('nomor_lengkap','kategori','sesi','masuk_penilaian','judul_gambar','gambar')

admin.site.register(Peserta, PesertaAdmin)