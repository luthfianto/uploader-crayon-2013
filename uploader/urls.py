from django.conf.urls import patterns, url
from uploader import views

urlpatterns = patterns('',
  url(r'^$', views.PesertaList.as_view(), name='peserta_list'),
  url(r'^new$', views.PesertaCreate.as_view(), name='peserta_new'),
  url(r'^centangs$', views.centangs ),
)