from django.db import models
import os

def path_and_rename(path):
    def wrapper(instance, filename):
        #dalam kasus ini, instance adalah nomor_lengkap
        #split menjadi ['filename','ext']. [-1] untuk ambil elemen belakang
        ext = filename.split('.')[-1]
        filename = '{}.{}'.format(instance, ext)

        return os.path.join(path, filename)
    return wrapper

#ImageField(upload_to=path_and_rename('upload/here/'), ...)

class Peserta(models.Model):
    KATEGORI_CHOICES = (
        ('A', 'A'),
        ('B', 'B'),
    )
    SESI_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
    )
    nomor_lengkap = models.CharField(max_length=30)
    sesi = models.PositiveSmallIntegerField(max_length=1, null=True, blank=True, choices=SESI_CHOICES, default=1)
    masuk_penilaian = models.NullBooleanField()
    gambar = models.ImageField(upload_to=path_and_rename('gambars/'), blank='unavailable.jpg')
    judul_gambar = models.CharField(max_length=30, null=True, blank=True)
    kategori = models.CharField(max_length=1,choices=KATEGORI_CHOICES, default='A')

    def __str__(self):
        return self.nomor_lengkap