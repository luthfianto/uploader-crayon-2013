from uploader.models import *
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import json
from django.forms import ModelForm
from django.contrib.messages.views import SuccessMessageMixin

class PesertaForm(ModelForm):
    class Meta:
        model = Peserta
        fields = ['nomor_lengkap', 'judul_gambar', 'gambar', 'sesi','kategori']

    def __init__(self, *args, **kwargs):
        super(PesertaForm, self).__init__(*args, **kwargs)
        self.fields['nomor_lengkap'].widget.attrs.update({'class': 'pure-u-1-2','required':''})
        self.fields['judul_gambar'].widget.attrs.update({'class': 'pure-u-1-2'})
        self.fields['gambar'].widget.attrs.update({'required':''})

class PesertaMixin:
    model = Peserta
    form_class = PesertaForm

class PesertaList(PesertaMixin, ListView):
    #pass
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_staff:
            PesertaList.template_name = 'uploader/peserta_list.html'
        else:
            return HttpResponseRedirect('/login')
        return super(PesertaList, self).dispatch(request, *args, **kwargs)

class PesertaCreate(PesertaMixin, CreateView,SuccessMessageMixin):
    success_url = reverse_lazy('peserta_new')
    success_message = "Upload sukses!"
    def get_success_message(self, cleaned_data):
        return self.success_message % dict(cleaned_data,
                                           calculated_field=self.object.calculated_field)

@csrf_exempt
def centangs(request):
    if request.POST:
        str=request.body.decode("utf-8")
        checkboxes=json.loads(str)
        for id in checkboxes['checked']:
            p=Peserta.objects.get(pk=id)
            p.masuk_penilaian=True
            p.save()

        for id in checkboxes['unchecked']:
            p=Peserta.objects.get(pk=id)
            p.masuk_penilaian=False
            p.save()

        return HttpResponse(200)
    return HttpResponse("POST at me bro")